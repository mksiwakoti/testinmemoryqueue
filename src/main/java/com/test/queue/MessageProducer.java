package com.test.queue;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TransferQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class MessageProducer implements Runnable {

    private TransferQueue<String> transferQueue;

    private String name;

    private Integer numberOfMessagesToProduce;

    public AtomicInteger numberOfProducedMessages = new AtomicInteger();

    public MessageProducer(TransferQueue<String> transferQueue, String name, int numberOfMessagesToProduce) {
        this.transferQueue = transferQueue;
        this.name = name;
        this.numberOfMessagesToProduce = numberOfMessagesToProduce;
    }

    @Override
    public void run() {
        for (int i = 0; i < numberOfMessagesToProduce; i++) {
            try {
                System.out.println("Producer: " + name + " is waiting to transfer...");
                boolean added = transferQueue.tryTransfer("A" + i, 4000, TimeUnit.MILLISECONDS);
                if (added) {
                    numberOfProducedMessages.incrementAndGet();
                    System.out.println("Producer: " + name + " transferred element: A" + i);
                } else {
                    System.out.println("can not add an element due to the timeout");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
