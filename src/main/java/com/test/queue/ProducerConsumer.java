package com.test.queue;

import java.util.Queue;
import java.util.SplittableRandom;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @author Madan Siwakoti
 */
public class ProducerConsumer {

    private static final int MAX_CAPACITY = 5;
    private static final Queue<String> jobQueue = new LinkedBlockingQueue<>(MAX_CAPACITY);

    public static void main(String[] args) throws InterruptedException {
        ProducerConsumer producerConsumer = new ProducerConsumer();
        for (int i = 0; i < 10; i++) {
            Thread t1 = new Thread(() -> {
                try {

                    producerConsumer.produce();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            t1.start();
        }
        for (int i = 0; i < 10; i++) {
            Thread t2 = new Thread(() -> {
                try {

                    producerConsumer.consume();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            t2.start();
        }


    }

    public void produce() throws InterruptedException {

        synchronized (this) {
            while (jobQueue.size() == MAX_CAPACITY) {
                System.out.println("queue is full, wait");
                wait();
            }

            SplittableRandom random = new SplittableRandom();
            String job = "Job" + random.nextInt();
            System.out.println("Producer produce -" + job);
            jobQueue.add(job);
            notify();

        }

    }

    public void consume() throws InterruptedException {

        synchronized (this) {
            while (jobQueue.size() == 0) {
                System.out.println("queeu is empty, wait");
                wait();
            }

            System.out.println("Consumer consume - " + jobQueue.remove());
            notify();


        }
    }
}
